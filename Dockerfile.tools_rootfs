FROM viryaos/sdk-qemu-user-aarch64:v3.7-45f9eeb-x86_64 AS sdk-qemu-user-aarch64

FROM arm64v8/alpine:3.12

COPY --from=sdk-qemu-user-aarch64 /home/builder/sdk-qemu-user-aarch64/qemu-aarch64 /usr/bin/qemu-aarch64

COPY --from=sdk-qemu-user-aarch64 /home/builder/sdk-qemu-user-aarch64/qemu-aarch64 /usr/bin/qemu-aarch64-static

COPY [ \
  "./output-viryaos-xen-tools/*", \
  "/" \
]

RUN \
  # apk
  apk update && \
  \
  # xen runtime deps
  apk add musl && \
  apk add openrc && \
  apk add busybox && \
  apk add sudo && \
  apk add dbus && \
  apk add bash && \
  apk add python2 && \
  apk add gettext && \
  apk add zlib && \
  apk add ncurses && \
  apk add texinfo && \
  apk add yajl && \
  apk add libaio && \
  apk add xz-dev && \
  apk add util-linux && \
  apk add argp-standalone && \
  apk add libfdt && \
  apk add glib && \
  apk add pixman && \
  apk add curl && \
  apk add udev && \
  \
  # Xen
  cd / && \
  tar xvzf viryaos-xen-tools.tar.gz && \
  rm viryaos-xen-tools.tar.gz && \
  \
  # Minimal ramdisk environment in case of cpio output
  rc-update add udev && \
  rc-update add udev-trigger && \
  rc-update add udev-settle && \
  rc-update add networking sysinit && \
  rc-update add loopback sysinit && \
  rc-update add bootmisc boot && \
  rc-update add devfs sysinit && \
  rc-update add dmesg sysinit && \
  rc-update add hostname boot && \
  rc-update add hwclock boot && \
  rc-update add hwdrivers sysinit && \
  rc-update add killprocs shutdown && \
  rc-update add modloop sysinit && \
  rc-update add modules boot && \
  rc-update add mount-ro shutdown && \
  rc-update add savecache shutdown && \
  rc-update add sysctl boot && \
  rc-update add local default && \
  cp -a /sbin/init /init && \
  echo "ttyS0" >> /etc/securetty && \
  echo "hvc0" >> /etc/securetty && \
  echo "ttyS0::respawn:/sbin/getty -L ttyS0 115200 vt100" >> /etc/inittab && \
  echo "hvc0::respawn:/sbin/getty -L hvc0 115200 vt100" >> /etc/inittab && \
  echo "/etc/init.d/xencommons start" > /etc/local.d/xencommons.start && \
  chmod +x /etc/local.d/xencommons.start && \
  rc-update add xencommons default && \
  passwd -d "root" root

