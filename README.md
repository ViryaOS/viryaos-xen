# viryaos-xen

This repository comes with several scripts and Dockerfiles to build the Xen
hypervisor, the Xen tools, and QEMU. They are all described in details
below.

First, use `Dockerfile.image` to build `viryaos-xen`: the "build container"
used for the build itself. Then you can use `viryaos-xen` to build Xen, the
Xen tools and QEMU for Alpine Linux, using the `main` script. The output
are two tarballs with the hypervisor and tools.

Finally, you can create an Alpine Linux rootfs with the Xen tools and their
runtime dependencies. The rootfs can be used as a chroot environment on any
distro, or as a cpio initrd and booted directly.


## `Dockerfile.image`

Build a container with all the build dependencies to build Xen and qemu-xen.
The container will be named "viryaos-xen".

```
$ cd viryaos-xen/

$ docker build --force-rm --file Dockerfile.image -t viryaos-xen .
```

## `main`

Use the build container to run `main` to build Xen and qemu-xen. The
output will be placed here under two directories named
"output-viryaos-xen-boot" and "output-viryaos-xen-tools".

```
$ docker run --rm -ti -v $(pwd):/home/builder/src viryaos-xen /home/builder/src/main
```

It is possible to choose a specific Xen git tree or QEMU git tree by
passing the XEN\_REPO and/or QEMU\_REPO environmental variables, and
making sure those directories are mapped into the container:

```
$ docker run -e XEN_REPO=/local/xen --rm -ti -v $(pwd):/home/builder/src -v /local/xen:/local/xen viryaos-xen /home/builder/src/main
```

It is also possible to avoid building QEMU by setting the `NOQEMU`
environmental variable to `y` or to any other value.

## `Dockerfile.tools_rootfs`

Use `Dockerfile.tools_rootfs` to create an Alpine Linux based rootfs with the
Xen tools and their runtime dependencies. The output is a container named
"viryaos-xen-tools-rootfs".

```
$ docker build --force-rm --file Dockerfile.tools_rootfs -t viryaos-xen-tools-rootfs .
```

The following commands are required to export the rootfs from the container to
a tarball and a cpio archive.

```
$ docker run -it -v `pwd`:/home/builder viryaos-xen-tools-rootfs \
    /home/builder/export.sh /home/builder/output-viryaos-xen-rootfs/xen-tools-rootfs
```

The output location and filename are specified by the last argument.

The resulting rootfs can be used as a simple chroot environment on any
Linux distro to run the Xen tools. To do that, you can use the tarball
produced in the previous step. For instance, you can untar it on the target
and use it as follows:

```
$ mkdir /chroot
$ cd /chroot
$ tar xvzf xen-tools-rootfs.tar.gz
$ cd /
$ mount -o bind /dev /chroot/dev
$ mount -o bind /proc /chroot/proc
$ mount -o bind /sys /chroot/sys
$ chroot /chroot
$ /etc/init.d/xencommons start
```


## `Dockerfile.package_tools`

Create a container package with the tools only.

```
$ docker build --force-rm --file Dockerfile.package_tools \
    -t viryaos-xen-package-tools .
```

## `Dockerfile.package_boot`

Create a container package with the hypervisor only.

```
$ docker build --force-rm --file Dockerfile.package_boot \
    -t viryaos-xen-package-boot .
```
