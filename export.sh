#!/bin/bash

# This script is meant to be run from a rootfs container built by
# Dockerfile.tools_rootfs.

output=$1
temp=`mktemp -d -p /mnt`

cp -r /init /bin /dev /etc /lib /media /root /sbin /usr /var $temp
rm $temp/usr/bin/qemu*
mkdir -p $temp/home
mkdir -p $temp/mnt
mkdir -p $temp/proc
mkdir -p $temp/run
mkdir -p $temp/srv
mkdir -p $temp/sys
mkdir -p $temp/tmp

mkdir -p `dirname $output`
cd $temp
tar cvzf "$output".tar.gz *
find . | cpio -H newc -o | gzip > "$output".cpio.gz

rm -rf $temp
